<?php

namespace Phalcon\Eyas\PlugIn\Mvc\View\Engine;

use Phalcon\DiInterface;
use Phalcon\Eyas\Traits\TraitContainer;
use Phalcon\Eyas\Traits\TraitGetParam;

/**
 * Class WattRoot
 * @package Phalcon\Eyas\Mvc\View\Engine
 */
class WattCommon extends WattExtension
{

    /**
     *
     */
    protected function onExecRepeat(
        $data,
        $elem,
        $function
    )
    {
        $_flag = true;

        $_html = '';

        while ($data --) {
            $_html.= $function($elem, $_flag);

            $_flag = false;
        }

        return $_html;
    }
}

/**
 * Class Watt
 * @package Phalcon\Eyas\Mvc\View\Engine
 */
class Watt extends \Phalcon\Mvc\View\Engine\Volt
{
    use TraitContainer,
        TraitGetParam;

    /*
     *
     */
    protected
        $Extension;

    /**
     *
     */
    protected function getDataProvider(
        $dataMixed,
        $extension,
        $elemDefault = null
    )
    {
        $_result = null;

        if (is_string($dataMixed)) {
            $dataMixed = $extension->getData($dataMixed);
        }

        foreach ($dataMixed as $I => $V) {
            $_result = (yield $I => $V);

            if ($_result === false) {
                break;
            }
        }

        while ($_result !== false) {
            $_result = (yield $elemDefault);
        }
    }

    /**
     *
     */
    protected function compileWatt(
        $elem,
        $extension = null,
        $list = null,
        $temp = null
    )
    {
        if ($list === null) {
            $list = $this->_options['wattsList'];
        }

        $_list = $this->containerGet('list' . $list);

        if ($_list === null) {
            $_list = $this->containerSet('list' . $list, json_decode(file_get_contents($this->_options['wattsPath'] . $list . '.json'), true));
        }

        if (is_array($elem) === false) {
            $elem = [ $elem ];
        }

        if ($extension === null) {
            $extension = $this->Extension;
        } else
        if (is_string($extension)) {
            $extension = $this->getExtension($extension);
        }

        $_html = '';

        foreach ($elem as $I => $V) {
            if (is_array($V)) {
                $_elem = $_list[$V[0]];
                $_type = $this->getParam('type', $_elem);

                switch ($_type) {
                    default:
                        $extension = $extension->create($this->getParam('#', $V), $extension);

                        $_func = function(
                          & $V,
                            $flag = false
                        ) use ($extension){
                            if (isset($V['$'])) {
                                if ($flag) {
                                    foreach ($V['$'] as $I =>&$Y) {
                                        if (isset($Y['='])) {
                                            $Y['#'] = $this->getDataProvider($Y['='], $extension);
                                        }
                                    }
                                }

                                return $this->compileWatt(
                                    $V['$'],
                                    $extension->create($this->getParam('#', $V), $extension)
                                );
                            }

                            return null;
                        };

                        if (isset($V['@']) === false) {
                            $V['@'] = 'single=';
                        }

                        $_temp = null;

                        foreach ($this->extract($V['@']) as $_comm => $_data) {
                            $_temp.= $extension->{ $_comm }($_data, $V, $_func);
                        }

                        if (isset($_elem['$'])) {
                            $_html.= $this->compileWatt(
                                $_elem['$'],
                                $extension,
                                null,
                                $_temp
                            );
                        }
                }
            } else {
                $extension->provide();

                switch (substr($V, 0, 4)) {
                    case '^^^^':
                        $_html.= $this->_compiler->compileString(substr($V, 4));

                        break;

                    default:
                        $_html.= preg_replace_callback(
                            '#\#<(.*?)>\##',
                            function($M) use ($extension, $temp) {
                                if ($M[1] === '@') {
                                    return $temp;
                                }

                                return $extension->{ $M[1] };
                            },
                            $V
                        );
                }
            }
        }

        return $_html;
    }

    /**
     *
     */
    protected function extract($command)
    {
        $_result = [];

        foreach (explode(';', $command) as $V) {
            $V = explode('=', $V);

            if (isset($V[1])) {
                $_result[$V[0]] = $V[1];
            }
        }

        return $_result;
    }

    /**
     *
     */
    protected function normalize(
        $expression,
      & $array = []
    )
    {
        foreach ($expression as $I => $V) {
            if (isset($V['name'])) {
                $I = $V['name'];
            }

            switch ($V['expr']['type']) {
                case 260:
                    $array[$I] = $V['expr']['value'];

                    break;

                case 360:
                    $array[$I] = [];

                    $this->normalize($V['expr']['left'], $array[$I]);
            }
        }

        return $array;
    }

    /**
     *
     */
    public function getExtension($class)
    {
        $class = '\\Phalcon\\Eyas\\Watt\\Custom\\' . $class;

        return $class::create();
    }

    /**
     *
     */
    public function setPrefix($value)
    {
        parent::setOptions([
            'compiledPath'  => $value . '.' . $this->_options['compiledPath'],
        ]);
    }

    /**
     *
     */
    public function setOptions(array $param)
    {
        if (is_array($param)) {
            if (isset($param['wattsList']) === false) {
                $param['wattsList'] = 'list';
            }

            parent::setOptions($param);
        }

        $_compiler = $this->getCompiler();

        $_compiler->addFunction(
            'ng',
            function($A, $E) {
                return '{{' . $E[0]['expr']['value'] . '}}';
            }
        );
        $_compiler->addFunction(
            'watt',
            function($A, $E) {
                $E = $this->normalize($E);

                if (count($E) < 1) {
                    throw new \Exception('Watt: parameter missed ( ' . $A . ' )');
                }

                return '"<!-- WATT -->"; ?>' . $this->compileWatt(
                    $this->getParam(0, $E),
                    $this->getParam(1, $E)
                ) . '<?php echo "<!-- WEND -->"';
            }
        );

        $this->Extension = WattCommon::create();
    }
}
