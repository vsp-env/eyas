<?php

namespace Phalcon\Eyas\PlugIn\Mvc\View\Engine;

use Phalcon\DiInterface;

/**
 * Class WattExtension
 * @package Phalcon\Eyas\Mvc\View\Engine
 */
class WattExtension extends \Phalcon\Mvc\User\Component
{

    /*
     *
     */
    protected
        $data,
        $root,

        $activeExtension,
        $activeMapper,

        $source;


    /**
     *
     */
    protected function __construct(
        $data = null,
        self $root = null
    )
    {
        $this->data = $data;
        $this->root = $root;
        $this->source = $data;

        if ($root) {
            if ($root->isMapper()) {
                $this->activeExtension = $root->getExtension();
            } else {
                $this->activeExtension = $root;
            }
        }
    }

    /**
     *
     */
    protected function onExecSingle(
        $data,
        $elem,
        $function
    )
    {
        return $function($elem);
    }

    /**
     *
     */
    public function __get($k)
    {
        return null;
    }

    /**
     *
     */
    public function __set($k, $v)
    {
        return $this->activeMapper ? $this->activeMapper->__set($k, $v) : null;
    }

    /**
     *
     */
    public function __call($k, $p)
    {
        if (method_exists($this, 'onExec' . $k)) {
            return $this->{ 'onExec' . $k }($p[0], $p[1], $p[2]);
        }

        return null;
    }

    /**
     *
     */
    protected function setMapper($value)
    {
        $this->activeMapper = $value;

        return $this;
    }

    /**
     *
     */
    public static function create(
        $data = null,
        self $root = null
    )
    {
        if ($root) {
            if (is_object($data) && $data instanceof \Generator === false) {
                return new WattExtensionObj($data, $root);
            }

            return new WattExtensionArr($data, $root);
        } else {
            if (is_object($data) && $data instanceof \Generator === false) {
                return new WattExtensionObj($data, new static());
            }

            return new WattExtensionArr($data, new static());
        }
    }

    /**
     *
     */
    public function getData($k)
    {
        if (method_exists($this, 'onData' . $k)) {
            return $this->{ 'onData' . $k }();
        }

        throw new \Exception('no data: ' . $k);
    }

    /**
     *
     */
    public function getExtension()
    {
        return $this;
    }

    /**
     *
     */
    public function isMapper()
    {
        return false;
    }

    /**
     *
     */
    public function data($data = null)
    {
        $this->data = $data;
        $this->source = $data;

        return $this;
    }

    /**
     *
     */
    public function provide()
    {
        if ($this->source instanceof \Generator) {
            $this->data = $this->source->current();

            $this->source->next();
        }
    }
}

/**
 * Class WattExtensionArr
 * @package Phalcon\Eyas\Mvc\View\Engine
 */
class WattExtensionArr extends WattExtension
{

    /**
     *
     */
    public function __get($k)
    {
        if (isset($this->data[$k])) {
            return $this->data[$k];
        }

        return $this->root->__get($k);
    }

    /**
     *
     */
    public function __set($k, $v)
    {
        if (is_array($this->data) === false) {
            $this->data = [];
        }

        return $this->data[$k] = $v;
    }

    /**
     *
     */
    public function __call($k, $p)
    {
        return $this->getExtension()->setMapper($this)->__call($k, $p);
    }

    /**
     *
     */
    public function getData($k)
    {
        return $this->getExtension()->getData($k);
    }

    /**
     *
     */
    public function getExtension()
    {
        return $this->activeExtension;
    }

    /**
     *
     */
    public function isMapper()
    {
        return true;
    }
}

/**
 * Class WattExtensionObj
 * @package Phalcon\Eyas\Mvc\View\Engine
 */
class WattExtensionObj extends WattExtension
{

    /**
     *
     */
    public function __get($k)
    {
        if (isset($this->data->{ $k })) {
            return $this->data->{ $k };
        }

        return $this->root->__get($k);
    }

    /**
     *
     */
    public function __set($k, $v)
    {
        if (is_object($this->data) === false) {
            $this->data = new \stdClass();
        }

        return $this->data->{ $k } = $v;
    }

    /**
     *
     */
    public function __call($k, $p)
    {
        return $this->getExtension()->setMapper($this)->__call($k, $p);
    }

    /**
     *
     */
    public function getData($k)
    {
        return $this->getExtension()->getData($k);
    }

    /**
     *
     */
    public function getExtension()
    {
        return $this->activeExtension;
    }

    /**
     *
     */
    public function isMapper()
    {
        return true;
    }
}
