<?php

namespace Phalcon\Eyas\PlugIn\Translation\Adapter;

use Phalcon\Eyas\PlugIn\Translation\Super;

/**
 * Class NativeInclude
 * @package Phalcon\Eyas\PlugIn\Translation\Adapter
 */
class NativeInclude extends Super
{

    /**
     *
     */
    public function onLoad($alias)
    {
        $config = $this->getConfig()->path;

        if (file_exists($config . $this->localization->getLocalization() . '.php')) {
            if ($alias) {
                $alias .= '.';
            }

            return include($config . $this->localization->getLocalization() . '.' . $alias . 'json');
        }

        return null;
    }
}
