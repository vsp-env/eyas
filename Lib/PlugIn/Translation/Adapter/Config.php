<?php

namespace Phalcon\Eyas\PlugIn\Translation\Adapter;

use Phalcon\Eyas\PlugIn\Translation\Super;

/**
 * Class Config
 * @package Phalcon\Eyas\PlugIn\Translation\Adapter
 */
class Config extends Super
{

    /**
     *
     */
    public function onLoad($alias)
    {
        return $this->getConfig();
    }
}
