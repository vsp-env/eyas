<?php

namespace Phalcon\Eyas\PlugIn\Translation\Adapter;

use Phalcon\Eyas\PlugIn\Translation\Super;

/**
 * Class Json
 * @package Phalcon\Eyas\PlugIn\Translation\Adapter
 */
class Json extends Super
{

    /**
     *
     */
    public function onLoad($alias)
    {
        $config = $this->getConfig()->path;

        if (file_exists($config . $this->localization->getLocalization() . '.json')) {
            if ($alias) {
                $alias .= '.';
            }

            return json_decode(file_get_contents($config . $this->localization->getLocalization() . '.' . $alias . 'json'), true);
        }

        return null;
    }
}
