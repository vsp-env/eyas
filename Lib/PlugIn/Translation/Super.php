<?php

namespace Phalcon\Eyas\PlugIn\Translation;

use Phalcon\Eyas\PlugIn\Bundle\Bundle;

/**
 * Class Super
 * @package Phalcon\Eyas\PlugIn\Translation
 */
abstract class Super extends Bundle
{

    /** @var mixed $data */
    public $data;

    /**
     *
     */
    abstract public function onLoad($alias);

    /**
     *
     */
    public function getConfig($dummy = null)
    {
        return parent::getConfig('translation');
    }

    /**
     *
     */
    public function _($k)
    {
        if ($this->data === null) {
            $this->data = $this->onLoad($this->dispatcher->getActiveController()->getTranslationAlias());
        }

        return isset($this->data[$k]) ? $this->data[$k] : '_' . $k . '_';
    }

    /**
     *
     */
    public function reload()
    {
        $this->onLoad($this->dispatcher->getActiveController()->getTranslationAlias());

        return $this;
    }
}
