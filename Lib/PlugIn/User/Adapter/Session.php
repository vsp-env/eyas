<?php

namespace Phalcon\Eyas\PlugIn\User\Adapter;

use Phalcon\Eyas\PlugIn\User\Super;

/**
 * Class Session
 * @package Phalcon\Eyas\PlugIn\User\Adapter\Session
 */
class Session extends Super
{

    /**
     *
     */
    public function getEncryptedId()
    {
        return $this->cookies->has('euid') ? trim($this->cookies->get('euid')->getValue()) : null;
    }

    /**
     *
     */
    public function setEncryptedId($id)
    {
        $this->cookies->set('euid', $id, $this->getConfig()->get('timeout', time() + 14 * 86400));

        return $this;
    }

    /**
     *
     */
    public function restore()
    {
        $_params = $this->session->get('params');

        if ($_params) {
            $this->data = $this->getParam('data', $_params);
            $this->id = $this->getParam('id', $_params);
            $this->role = $this->getParam('role', $_params);

            return $this->role ? $this->onRestore() : false;
        }

        return $this->getConfig()->offline
            ? ($this->getEncryptedId()
                ? ($this->authenticateById($this->getEncryptedId()) && $this->onRestore())
                : false)
            : false;
    }

    /**
     *
     */
    public function save($force = false)
    {
        if (($this->role || $force)) {
            $this->session->set('params', [
                'data' => $this->data,
                'id' => $this->id,
                'role' => $this->role,
            ]);

            return $this->getConfig()->offline ? $this->setEncryptedId($this->id)->onSave() : $this->onSave();
        }

        return false;
    }
}
