<?php

namespace Phalcon\Eyas\PlugIn\User;

use Phalcon\DiInterface;
use Phalcon\Eyas\PlugIn\Bundle\Bundle;

/**
 * Class Super
 * @package Phalcon\Eyas\PlugIn\User
 */
abstract class Super extends Bundle
{

    /** @var mixed $data */
    public $data;

    /** @var null $id */
    public $id = null;

    /** @var bool $isAuthorized */
    public $isAuthorized = false;

    /** @var bool $isGuest */
    public $isGuest = true;

    /** @var int $role */
    public $role = 0;

    /** @var null $user */
    public $user = null;

    /**
     *
     */
    public function onRestore()
    {
        $this->isGuest = ! $this->isAuthorized = true;

        return true;
    }

    /**
     *
     */
    public function onSave()
    {
        $this->isGuest = ! $this->isAuthorized = true;

        return true;
    }

    /**
     *
     */
    public function __construct()
    {
        $this->restore();
    }

    /**
     *
     */
    public function getConfig($dummy = null)
    {
        return parent::getConfig('user');
    }

    /**
     *
     */
    public function get()
    {
        return $this->user;
    }

    /**
     *
     */
    public function set($value = null)
    {
        $this->user = $value;

        return $this;
    }

    /**
     *
     */
    public function getData($k = null)
    {
        return $k ? $this->getParam($k, $this->data) : $this->data;
    }

    /**
     *
     */
    public function setData($v)
    {
        $this->data = $v;

        return $this;
    }

    /**
     *
     */
    public function getDataKey($k, $valueDefault = null)
    {
        return $this->getParam($k, $this->data, $valueDefault);
    }

    /**
     *
     */
    public function setDataKey($k, $v)
    {
        is_array($this->data) ? $this->data[$k] = $v : $this->data = [ $k => $v ];

        return $this;
    }

    /**
     *
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     */
    public function setId($value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     *
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     *
     */
    public function setRole($value = null)
    {
        $this->role = $value;

        return $this;
    }

    /**
     *
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     *
     */
    public function setUser($value = null)
    {
        $this->user = $value;

        return $this;
    }

    /**
     *
     */
    public function authenticateByField($value)
    {
        $this->user = $this->application->onUserByField($value);

        $this->sync()->save(true);

        return empty($this->user) === false;
    }

    /**
     *
     */
    public function authenticateById($id = null)
    {
        $this->user = $this->application->onUserById($id ? $id : $this->id);

        $this->sync()->save(true);

        return empty($this->user) === false;
    }

    /**
     *
     */
    public function byField($value)
    {
        return $this->application->onUserByField($value, $this);
    }

    /**
     *
     */
    public function byId($id = null)
    {
        return $this->application->onUserById($id ? $id : $this->id, $this);
    }

    /**
     *
     */
    public function clear()
    {
        return $this->setUser(null)->sync()->save(true);
    }

    /**
     *
     */
    abstract public function restore();

    /**
     *
     */
    abstract public function save($force = false);

    /**
     *
     */
    public function sync()
    {
        if ($this->user instanceof UserIdentityInterface) {
            $this->data = $this->user->getData();
            $this->isGuest = false;
            $this->isAuthorized = true;
            $this->id = $this->user->getId();
            $this->role = $this->user->getRole();
        } else {
            $this->data = null;
            $this->isGuest = true;
            $this->isAuthorized = false;
            $this->id = null;
            $this->role = null;
        }

        return $this;
    }

}
