<?php

namespace Phalcon\Eyas\PlugIn\User;

/**
 * Interface UserIdentityInterface
 * @package Phalcon\Eyas\PlugIn\User
 */
interface UserIdentityInterface
{

    function getData();

    function getId();

    function getRole();

}
