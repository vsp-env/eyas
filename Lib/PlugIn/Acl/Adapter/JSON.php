<?php

namespace Phalcon\Eyas\PlugIn\Acl\Adapter;

use Phalcon\Eyas\PlugIn\Acl\Super;

/**
 * Class Json
 * @package Phalcon\Eyas\PlugIn\Acl\Adapter
 */
class Json extends Super
{

    /**
     *
     */
    public function onLoad($alias)
    {
        return json_decode(file_get_contents($this->getKey('file')));
    }
}
