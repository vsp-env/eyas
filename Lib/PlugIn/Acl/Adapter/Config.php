<?php

namespace Phalcon\Eyas\PlugIn\Acl\Adapter;

use Phalcon\Eyas\PlugIn\Acl\Super;

/**
 * Class Config
 * @package Phalcon\Eyas\PlugIn\Acl\Adapter
 */
class Config extends Super
{

    /**
     *
     */
    public function onLoad($alias)
    {
        return $this->getConfig();
    }
}
