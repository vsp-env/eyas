<?php

namespace Phalcon\Eyas\PlugIn\Acl\Adapter;

use Phalcon\Eyas\PlugIn\Acl\Super;

/**
 * Class NativeInclude
 * @package Phalcon\Eyas\PlugIn\Acl\Adapter
 */
class NativeInclude extends Super
{

    /**
     *
     */
    public function onLoad($alias)
    {
        return include($this->getKey('file'));
    }
}
