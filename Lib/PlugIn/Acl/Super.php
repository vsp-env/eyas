<?php

namespace Phalcon\Eyas\PlugIn\Acl;

use Phalcon\Eyas\PlugIn\Bundle\Bundle;

/**
 *
 */
abstract class Super extends Bundle
{

    public $data;

    public $selectedRule;

    /**
     *
     */
    protected function check($controller, $action, $dispatcher)
    {
        if (isset($this->data[$controller])) {
            $roleList = null;

            // custom controller handler
            if (is_callable($this->data[$controller])) {
                $_method = $this->data[$controller];

                return $this->selectedRule = $_method($controller, $action, $this->user, $dispatcher);
            }

            // try by action name then default
            if (isset($this->data[$controller][$action])) {
                $roleList = $this->data[$controller][$action];
            } else
            if (isset($this->data[$controller]['*'])) {
                $roleList = $this->data[$controller]['*'];
            }

            if ($roleList) {
                $role = null;

                if (isset($roleList[$this->user->getRole()])) {
                    $role = $roleList[$this->user->getRole()];
                } else
                if (isset($roleList['*'])) {
                    $role = $roleList['*'];
                }

                if ($role) {
                    return $this->selectedRule = is_callable($role)
                        ? $role($controller, $action, $this->user, $dispatcher)
                        : true;
                }
            }
        }

        return false;
    }

    /**
     *
     */
    abstract public function onLoad($alias);

    /**
     *
     */
    public function getConfig($dummy = null)
    {
        return parent::getConfig('acl');
    }

    /**
     *
     */
    public function initialize()
    {
        $this->data = $this->onLoad(null);

        $eventManager = $this->eventsManager;

        $eventManager->attach('dispatch', function ($event, $dispatcher) {
            if ($event->getType() === 'beforeExecuteRoute') {

                // check exactly by controller name
                $check = $this->check(
                    $dispatcher->getControllerName(),
                    $dispatcher->getActionName(),
                    $dispatcher
                );

                // if check op must be self handled (null) or check is success
                if ($check === null || $check) {
                    return $check === true;
                }

                // check by '*' as default set of rules
                $check = $this->check(
                    '*',
                    $dispatcher->getActionName(),
                    $dispatcher
                );

                // if check op must be self handled (null) or check is success
                if ($check === null || $check) {
                    return $check === true;
                }

                // if no rules found
                $config = $this->getConfigApp();

                if ($check !== true && isset($config->application->preferences->defaultRouteOnAccessDenied)) {
                    $this->dispatcher->setActionName($config->application->preferences->defaultRouteOnAccessDenied->action);
                    $this->dispatcher->setControllerName($config->application->preferences->defaultRouteOnAccessDenied->controller);
                    $this->dispatcher->dispatch();
                }

                return false;
            }
        });

        $this->dispatcher->setEventsManager($eventManager);
    }
}
