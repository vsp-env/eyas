<?php

namespace Phalcon\Eyas\PlugIn\Localization;

use Phalcon\Eyas\PlugIn\Bundle\Bundle;

/**
 * Class Localization
 * @package Phalcon\Eyas\PlugIn\Localization
 */
class Localization extends Bundle
{

    /** @var null|string $locale */
    public $locale = null;

    /** @var null|string $localization */
    public $localization = null;

    /**
     *
     */
    public function getConfig($dummy = null)
    {
        return parent::getConfig('localization');
    }

    /**
     *
     */
    public function getLocale()
    {
        if ($this->locale) {
            return $this->locale;
        }

        if (function_exists('geoip_country_code_by_name')) {
            $result = geoip_country_code_by_name($this->request->getClientAddress());
        } else {
            $result = null;
        }

        return $this->locale = strtolower($result);
    }

    /**
     *
     */
    public function getLocalization($value = null)
    {
        if ($value === null) {
            if ($this->localization) {
                return $this->localization;
            }

            if ($this->cookies->has('localization')) {
                $result = $this->cookies->get('localization')->getValue();
            } else {
                $result = null;
            }
        } else {
            $result = $value;
        }

        $config = $this->getConfig();

        if (empty($result) || isset($config->list->{ $result }) === false) {
            $locale = $this->getLocale();

            if ($locale && isset($config->geo->map->{ $locale })) {
                $result = $config->geo->map->{ $locale };
            } else {
                $result = $config->default;
            }
        }

        $this->cookies->set('localization', strtolower($result));

        return $this->localization = $result;
    }
}
