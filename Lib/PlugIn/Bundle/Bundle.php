<?php

namespace Phalcon\Eyas\PlugIn\Bundle;

use Phalcon\DiInterface;
use Phalcon\Eyas\Eyas\EyasModule;

/**
 * Class Bundle
 * @package Phalcon\Eyas\PlugIn\Bundle
 */
abstract class Bundle extends EyasModule
{

    /** @var mixed $_client */
    protected $_client;

    /*
     *
     */
    protected function onGetClient($alias)
    {
        return null;
    }

    /*
     *
     */
    public function getClient($alias = null)
    {
        if ($this->_client === null) {
            $this->_client = $this->onGetClient($alias);
        }

        return $this->_client;
    }

    /*
     *
     */
    public function getConfig($alias = null)
    {
        return parent::getConfig('application')->bundles->{ $alias };
    }

    /*
     *
     */
    public function getConfigApp()
    {
        return $this->configApp;
    }
}
