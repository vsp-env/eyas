<?php

namespace Phalcon\Eyas\Mvc\Controller;

use Phalcon\Eyas\Mvc\Controller\Traits\TraitScope;

/**
 * Class ControllerCommander
 * @package Phalcon\Eyas\Mvc\Controller
 */
abstract class SimpleCommander extends Simple
{
    use TraitScope;

    /** @var array $_optCommanderScopes */
    protected static $_optCommanderScopes = [];

    /**
     *
     */
    public function initialize()
    {
        $this->n->adaptJSON();
    }

    /**
     *
     */
    public function error403()
    {
        return $this->n->respondCodeForbidden();
    }

    /**
     *
     */
    public function error404()
    {
        return $this->n->respondCodeNotFound();
    }

    /**
     *
     */
    public function error500()
    {
        return $this->n->respondCodeInternalServerError();
    }

    /**
     *
     */
    public function commandAction($command = 'index')
    {
        $_action = $this->n->_('scopeCommand', $command);

        if ($_action == 'index' || $_action == 'scope') {
            $_action =
                $_action .
                $this->n->get('scopeIndexInitiator');
        } else {
            $_action =
                'command' .
                $_action;
        }

        if (method_exists($this, $_action . 'Action')) {
            $this->forward($_action);
        } else {
            $this->error404();
        }
    }
}
