<?php

namespace Phalcon\Eyas\Mvc\Controller;

use Phalcon\Eyas\Mvc\Controller\Extra\View;

/**
 * Class E
 *
 * @package Phalcon\Eyas\Mvc\Controller
 */
abstract class E extends Simple
{

    /** @var bool $_optDisableLayout */
    protected static $_optDisableLayout = false;

    /**
     *
     */
    protected function onInitV()
    {
        return new View($this);
    }

    /**
     *
     */
    public function initialize()
    {
        if (static::$_optDisableLayout) {
            $this->view->disableLevel(\Phalcon\Mvc\View::LEVEL_MAIN_LAYOUT);
            $this->view->disableLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
        }
    }

    /**
     *
     */
    public function error403Action()
    {
        $this->n->respondCodeForbidden();
    }

    /**
     *
     */
    public function error404Action()
    {
        $this->n->respondCodeNotFound();
    }

    /**
     *
     */
    public function error500Action()
    {
        $this->n->respondCodeInternalServerError();
    }
}
