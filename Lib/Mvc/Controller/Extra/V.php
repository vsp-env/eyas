<?php

namespace Phalcon\Eyas\Mvc\Controller\Extra;

use Phalcon\Eyas\Extra\Loader\Loader;
use Phalcon\Eyas\Mvc\Controller\Extra\Base\Base;
use Phalcon\Eyas\Traits\TraitContainer;

/**
 * Class V
 * @package Phalcon\Eyas\Mvc\Controller\Extra
 */
class V extends Base
{

    /** @var int $ajax */
    protected $ajax = 2;

    /** @var bool|int $level  */
    protected $level = true;

    /*
     *
     */
    public function set($alias = null)
    {
        $_entity = $this->dispatcher;

        return $this->setDirect(
            $_entity->getControllerName() . '/' . ($alias === null
                ? $_entity->getActionName()
                : $alias
            )
        );
    }

    /*
     *
     */
    public function setReferenced($alias = null)
    {
        $_entity = $this->dispatcher;

        return $this->setDirect(
            $this->router->getModuleName() . '/' . $_entity->getControllerName() . '/' . ($alias === null ?
                $_entity->getActionName() :
                $alias
            )
        );
    }

    /*
     *
     */
    public function setDirect($alias)
    {
        if ($this->level === true || $this->ajax === $this->level) {
            $this->getController()->view->pick($alias);

            $this->ajax = 3;
        }

        return $this;
    }

    /*
     *
     */
    public function setLevel($value)
    {
        $this->level = $value;

        return $this;
    }

    /*
     *
     */
    public function setParams(array $value)
    {
        $this->getController()->view->setVars($value, true);

        return $this;
    }

    /*
     *
     */
    public function setResult($alias = null)
    {
        $this->set($alias !== null ? $alias . '.result' : $alias);

        return $this;
    }

    /**
     *
     */
    public function disable()
    {
        $this->getController()->view->disable();

        return $this;
    }

    /**
     *
     */
    public function enable()
    {
        $this->getController()->view->enable();

        return $this;
    }

    /**
     *
     */
    public function enableAction()
    {
        $this->getController()->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);

        return $this;
    }

    /**
     *
     */
    public function enableLayout()
    {
        $this->getController()->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);

        return $this;
    }

    /**
     *
     */
    public function enableMainLayout()
    {
        $this->getController()->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_MAIN_LAYOUT);

        return $this;
    }

    /**
     *
     */
    public function prefix($value)
    {
        $this->loaderEyas->getInject()->getConfig()->viewsDir.= $value . '_';

        return $this;
    }

    /**
     *
     */
    public function prefixCache($value)
    {
        $this->loaderEyas->getInject()->getConfig()->cacheDir.= $value . '_';

        return $this;
    }

    /**
     *
     */
    public function prepareTranslation()
    {
        $this->getController()->view->t = $this->getDI()->getTranslation();

        return $this;
    }
}
