<?php

namespace Phalcon\Eyas\Mvc\Controller\Extra;

use Phalcon\Eyas\Mvc\Controller\Extra\Base\Base;

/**
 * Class N
 * @package Phalcon\Eyas\Mvc\Controller\Extra
 */
class N extends Base
{

    /*
     *
     */
    protected
        $_getter,
        $_getterEntire,
        $_setter,
        $_setterEntire,
        $_defaultValue = null,
        $_filter = FILTER_SANITIZE_SPECIAL_CHARS,
        $_template;

    /**
     *
     */
    public function __construct($config)
    {
        parent::__construct($config);

        $this->adaptPost();
    }

    /**
     *
     */
    public function get($alias)
    {
        $_getter = $this->_getter;

        return $_getter($alias, $this->_defaultValue, $this->_filter);
    }

    /**
     *
     */
    public function getEntire()
    {
        $_getter = $this->_getterEntire;

        return $_getter();
    }

    /**
     *
     */
    public function getNotFiltered($alias)
    {
        $_result = $this->get($alias, null, null);

        return $_result;
    }

    /**
     *
     */
    public function setDefault($value = null)
    {
        $this->_defaultValue = $value;

        return $this;
    }

    /**
     *
     */
    public function setFilter($value = FILTER_SANITIZE_SPECIAL_CHARS)
    {
        $this->_filter = $value;

        return $this;
    }

    /**
     *
     */
    public function getIP()
    {
        return $this->request->getClientAddress();
    }

    /**
     *
     */
    public function _($alias, $valueDefault = null)
    {
        $v =&$this->_template;
        $i = 0;

        while (($p = strpos($alias, '.', $i)) !== false) {
            $i = substr($alias, $i, $p - $i);

            if (is_array($v) && isset($v[$i])) {
                $v =&$v[$i];
            } else
            if (is_object($v) && isset($v->$i)) {
                $v =&$v->$i;
            } else {
                return $valueDefault;
            }

            $i = $p + 1;
        }

        $i = substr($alias, $i);

        if (is_array($v)) {
            return isset($v[$i]) ? $v[$i] : $valueDefault;
        }

        if (is_object($v)) {
            return isset($v->$i) ? $v->$i : $valueDefault;
        }

        return $valueDefault;
    }

    /*
     *
     */
    public function adaptGet()
    {
        $this->_getter = function($a, $d = null, $f = null) {
            if ($this->request->has($a)) {
                $d = $this->request->get($a);

                if ($f !== null) {
                    return filter_var($d, $f);
                }
            }

            return $d;
        };

        $this->_getterEntire = function() {
            return $_GET;
        };

        $this->_filter = FILTER_SANITIZE_SPECIAL_CHARS;
        $this->_template = null;

        return $this;
    }

    /*
     *
     */
    public function adaptJSON()
    {
        $this->_getter = function($a, $d = null, $f = null) {
            if (isset($this->_template->{ $a })) {
                $d = $this->_template->{ $a };

                if ($f !== null) {
                    return filter_var($d, $f);
                }
            }

            return $d;
        };

        $this->_getterEntire = function() {
            return $this->_template;
        };

        $this->_filter = null;
        $this->_template = $this->request->getJsonRawBody();

        return $this;
    }

    /*
     *
     */
    public function adaptPost()
    {
        $this->_getter = function($a, $d = null, $f = null) {
            if ($this->request->hasPost($a)) {
                $d = $this->request->getPost($a);

                if ($f !== null) {
                    return filter_var($d, $f);
                }
            }

            return $d;
        };

        $this->_getterEntire = function() {
            return $_POST;
        };

        $this->_filter = FILTER_SANITIZE_SPECIAL_CHARS;
        $this->_template = null;

        return $this;
    }

    /*
     *
     */
    public function g($k, $d = null, $f = FILTER_SANITIZE_SPECIAL_CHARS)
    {
        if ($this->request->hasQuery($k)) {
            $d = $this->request->getQuery($k);

            if ($f !== null) {
                return filter_var($d, $f);
            }
        }

        return $d;
    }

    /*
     *
     */
    public function p($k, $d = null, $f = FILTER_SANITIZE_SPECIAL_CHARS)
    {
        if ($this->request->hasPost($k)) {
            $d = $this->request->getPost($k);

            if ($f !== null) {
                return filter_var($d, $f);
            }
        }

        return $d;
    }

    /**
     *
     */
    public function redirect($route = null, $param = null)
    {
        if (is_array($route)) {
            $route = $this->url->get($route);
        } else
            if ($route === null) {
                $route = $this->dispatcher->getControllerName() . '/' . $this->dispatcher->getActionName();
            } else
            if ($param === true) {
                $route = $this->dispatcher->getControllerName() . '/' . $route;

                $param = null;
            }

        if (is_array($param)) {
            $param = '/' . implode('/', $param);
        }

        return $this->response->redirect($route . $param);
    }

    /**
     *
     */
    public function respond($data, $type = null, $code = null, $codeMessage = null)
    {
        if ($type !== null) {
            $this->response->setContentType($type, 'UTF-8');
        }

        if ($code !== null) {
            $this->response->setStatusCode($code, $codeMessage);
        }

        $this->response->setContent($data);

        return $this->response;
    }

    /**
     *
     */
    public function respondCode($code, $codeMessage, $data = null)
    {
        $this->response->setStatusCode($code, $codeMessage);
        $this->response->setContent($data);

        return $this->response;
    }

    /**
     *
     */
    public function respondCodeBadRequest($codeMessage = null)
    {
        return $this->respond(
            null,
            null,
            400,
            $codeMessage ? $codeMessage : 'Bad request.'
        );
    }

    /**
     *
     */
    public function respondCodeForbidden($codeMessage = null)
    {
        return $this->respond(
            null,
            null,
            403,
            $codeMessage ? $codeMessage : 'Forbidden.'
        );
    }

    /**
     *
     */
    public function respondCodeInternalServerError($codeMessage = null)
    {
        return $this->respond(
            null,
            null,
            500,
            $codeMessage ? $codeMessage : 'Internal server error.'
        );
    }

    /**
     *
     */
    public function respondCodeNotFound($codeMessage = null)
    {
        return $this->respond(
            null,
            null,
            404,
            $codeMessage ? $codeMessage : 'Not found.'
        );
    }

    /**
     *
     */
    public function respondJSON($data)
    {
        return $this->respond(
            json_encode($data),
            'application/json'
        );
    }

    /*
     *
     */
    public function scope($alias, $force = false)
    {
        if ($force) {
            return true;
        }

        $result = $this->get('scope', null, null);

        if (is_object($result) && isset($result->{ $alias })) {
            return $result->{ $alias };
        }

        return null;
    }
}
