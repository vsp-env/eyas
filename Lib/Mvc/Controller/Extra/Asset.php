<?php

namespace Phalcon\Eyas\Mvc\Controller\Extra;

use Phalcon\Eyas\Mvc\Controller\Extra\Base\Base;

/**
 * Class Asset
 * @package Phalcon\Eyas\Mvc\Controller\Extra
 */
class Asset extends Base
{

    /**
     *
     */
    public function css($alias, $collection = null)
    {
        if ($collection) {
            $this->_client->assets->collection($collection);
        }

        if (is_array($alias)) {
            foreach ($alias as $v) {
                $this->_client->assets->addCss($v, true);
            }
        } else {
            $this->_client->assets->addCss($alias, true);
        }

        return $this;
    }

    /**
     *
     */
    public function cssRemote($alias, $collection = null)
    {
        if ($collection) {
            $this->_client->assets->collection($collection);
        }

        if (is_array($alias)) {
            foreach ($alias as $V) {
                $this->_client->assets->addCss($V);
            }
        } else {
            $this->_client->assets->addCss($alias);
        }

        return $this;
    }

    /**
     *
     */
    public function js($alias, $collection = null)
    {
        if ($collection) {
            $this->_client->assets->collection($collection);
        }

        if (is_array($alias)) {
            foreach ($alias as $v) {
                $this->_client->assets->addJs($v);
            }
        } else {
            $this->_client->assets->addJs($alias);
        }

        return $this;
    }

    /**
     *
     */
    public function jsRemote($alias, $collection = null)
    {
        if ($collection) {
            $this->_client->assets->collection($collection);
        }

        if (is_array($alias)) {
            foreach ($alias as $v) {
                $this->_client->assets->addJs($v);
            }
        } else {
            $this->_client->assets->addJs($alias);
        }

        return $this;
    }
}
