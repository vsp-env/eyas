<?php

namespace Phalcon\Eyas\Mvc\Controller\Extra\Base;

use Phalcon\Eyas\Eyas\EyasModule;
use Phalcon\Eyas\Traits\TraitContainer;
use Phalcon\Eyas\Traits\TraitGetterSetterPhalcon;

/**
 * Class Base
 * @package Phalcon\Eyas\Mvc\Controller\Extra
 */
abstract class Base extends EyasModule
{
    use TraitContainer;

    /*
     *
     */
    public function __construct($config = null)
    {
        $this->_config = $config;

        $this->initialize();
    }

    /*
     *
     */
    public function getController()
    {
        return $this->dispatcher->getActiveController();
    }

    /*
     *
     */
    public function getDispatcher()
    {
        return $this->dispatcher;
    }
}
