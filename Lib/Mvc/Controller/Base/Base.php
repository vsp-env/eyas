<?php

namespace Phalcon\Eyas\Mvc\Controller\Base;

use Phalcon\Eyas\Traits\TraitConfig;
use Phalcon\Eyas\Traits\TraitGetParam;
use Phalcon\Eyas\Traits\TraitGetterSetterPhalcon;
use Phalcon\Eyas\Traits\TraitWhoAmI;

/**
 * Class Base
 * @package Phalcon\Eyas\Mvc\Controller
 *
 * @property \Phalcon\Eyas\PlugIn\User\Super user
 */
abstract class Base extends \Phalcon\Mvc\Controller
{
    use TraitConfig,
        TraitGetterSetterPhalcon,
        TraitGetParam,
        TraitWhoAmI;

    /**
     *
     */
    public function getBundle($alias, $param = null)
    {
        $alias = 'bundle' . $alias;

        if ($this->getDI()->has($alias)) {
            return $this->getDI()->get($alias);
        }

        $class = static::whoIsMyNS(- 11) . 'Bundles\\' . $alias;
        $class = new $class($this, $param);

        $this->getDI()->setShared($alias, $class);

        return $class;
    }

    /**
     *
     */
    public function forward($action, $controller = null, $module = null, array $param = [])
    {
        $param = [
            'action' => $action,
            'controller' => $controller !== null ? $controller : $this->dispatcher->getControllerName(),
            'params' => $param,
        ];

        return $this->dispatcher->forward($param);
    }
}
