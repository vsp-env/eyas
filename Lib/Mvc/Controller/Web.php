<?php

namespace Phalcon\Eyas\Mvc\Controller;

use Phalcon\Eyas\Mvc\Controller\Extra\V;

/**
 * Class Web
 *
 * @property \Phalcon\Eyas\Mvc\Controller\Extra\N n
 * @property \Phalcon\Eyas\Mvc\Controller\Extra\V v
 *
 * @package Phalcon\Eyas\Mvc\Controller
 */
abstract class Web extends Simple
{

    const
        REQ_NORMAL = 0,
        REQ_RESERVED = 1,
        REQ_AJAX = 2,
        REQ_AJAX_PROCESSED = 3;

    /**
     *
     */
    protected function onInitV()
    {
        return new V($this);
    }

    /**
     *
     */
    public function initialize()
    {
        if ($this->request->isAjax()) {
            $this->_ajax = self::REQ_AJAX;

            $this->view->disableLevel(\Phalcon\Mvc\View::LEVEL_MAIN_LAYOUT);
            $this->view->disableLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
        }

        $this->view->me = $this;
    }
}
