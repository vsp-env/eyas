<?php

namespace Phalcon\Eyas\Mvc\Controller;

use Phalcon\Eyas\Mvc\Controller\Base\Base;
use Phalcon\Eyas\Mvc\Controller\Extra\N;

/**
 * Class Controller
 *
 * @property \Phalcon\Eyas\Mvc\Controller\Extra\N n
 * @property \Phalcon\Eyas\Mvc\Controller\Extra\V v
 *
 * @package Phalcon\Eyas\Mvc\Controller
 */
abstract class Simple extends Base
{

    /** @var null $_optTranslationAlias */
    protected static $_optTranslationAlias = null;

    /** @var int $_ajax */
    protected $_ajax = 0;

    /** @var null $_scrf */
    protected $_scrf = null;

    /**
     *
     */
    protected function onInitApplication()
    {
        return $this->getDI()->getApplication();
    }

    /**
     *
     */
    protected function onInitN()
    {
        return new N($this);
    }

    /**
     *
     */
    public function getTranslationAlias()
    {
        return static::$_optTranslationAlias === true ? static::whoAmI() : static::$_optTranslationAlias;
    }

    /**
     *
     */
    public function error403()
    {
        return $this->forward('error403', 'error');
    }

    /**
     *
     */
    public function error404()
    {
        return $this->forward('error404', 'error');
    }

    /**
     *
     */
    public function error500()
    {
        return $this->forward('error500', 'error');
    }
}
