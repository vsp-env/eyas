<?php

namespace Phalcon\Eyas\Mvc\Controller\Traits;

/**
 * Class TraitControllerScope
 * @package Phalcon\Eyas\Mvc\Controller\Traits
 */
trait TraitScope
{

    /** @var bool $optScopeForce */
    protected $optScopeForce = false;
    
    /** @var null $optScopeModel */
    protected $optScopeModel = null;
    
    /** @var array $scopeResponse */
    protected $scopeResponse = [ 'scope' => [] ];

    /*
     *
     */
    protected function scopeForce($value = true)
    {
        $this->optScopeForce = $value;

        return $this;
    }

    /*
     *
     */
    protected function scopeModel($value = true)
    {
        $this->optScopeModel = $value;

        return $this;
    }

    /*
     *
     */
    protected function scopeRespond()
    {
        $_result = $this->n->respondJSON($this->scopeResponse);

        return $_result;
    }

    /*
     *
     */
    protected function scopeStatusAdd($k, $v)
    {
        $this->scopeResponse['scope'][$k] = $v;

        return $this;
    }

    /*
     *
     */
    protected function scopeValue($v, $mixed = null)
    {
        return is_callable($v) ?
            $v($mixed) :
            $v;
    }

    /*
     *
     */
    protected function scopePrepareCallback($alias, $value = null, $aliasScope = null)
    {
        if ($aliasScope === null) {
            $_alias = $alias;
        } else {
            $_alias = $aliasScope;
        }

        $_scope = $this->n->scope($alias);

        if ($this->optScopeForce || $_scope) {
            $this->scopeResponse[$_alias] = method_exists($this, 'onScope' . $alias) ? $this->scopeValue($this->{ 'onScope' . $alias }($value), $_scope) : null;
        }

        return $this;
    }

    /*
     *
     */
    protected function scopePrepareRaw($alias, $value, $aliasScope = null)
    {
        if ($aliasScope === null) {
            $_alias = $alias;
        } else {
            $_alias = $aliasScope;
        }

        $_scope = $this->n->scope($alias);

        if ($this->optScopeForce || $_scope) {
            $this->scopeResponse[$_alias] = $this->scopeValue($value, $_scope);
        }

        return $this;
    }
}
