<?php

namespace Phalcon\Eyas\Traits;

/**
 * Class TraitGetParam
 * @package Phalcon\Eyas\Traits
 */
trait TraitGetParam
{

    /**
     * Get value from
     *
     * @param string $alias Alias of access
     * @param array $param Container reference
     * @param null $valueDefault Value will be returned by default, if aliased value was not found
     *
     * @return null
     */
    public static function getParam($alias, & $param, $valueDefault = null)
    {
        return isset($param[$alias]) ? $param[$alias] : $valueDefault;
    }

    /**
     * Get value reference from
     *
     * @param string $alias Alias of access
     * @param array $param Container reference
     * @param null $valueDefault Value will be set and returned by default, if aliased value was not found
     *
     * @return mixed
     */
    public static function&getParamRefer($alias, & $param, $valueDefault = null)
    {
        if (! isset($param[$alias])) {
            $param[$alias] = $valueDefault;
        }

        return $param[$alias];
    }
}
