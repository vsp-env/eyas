<?php

namespace Phalcon\Eyas\Traits;

/**
 * Class TraitGetterSetterPhalcon
 * @package Phalcon\Eyas\Traits
 */
trait TraitGetterSetterPhalcon
{

    /** @var int $_inGetterSetterPhalconValue */
    private $_inGetterSetterPhalconValue = 0;

    /**
     *
     */
    public function __get($k)
    {
        return $this->getDI()->has($k) ? $this->getDI()->getShared($k) : $this->__set($k, $this->{ 'onInit' . $k }());
    }

    /**
     *
     */
    public function __set($k, $v)
    {
        return $this->{ $k } = $v;
    }
}
