<?php

namespace Phalcon\Eyas\Traits;

/**
 * Class TraitSingleton
 * @package Phalcon\Eyas\Traits
 */
trait TraitSingleton
{

    /** @var null $_inInstance */
    private static $_inInstance = null;

    /**
     *
     */
    final private function __construct($param = null)
    {
        if (method_exists($this, 'onInstance')) {
            call_user_func_array(
                'static::onInstance',
                is_array($param) ?
                    $param :
                    func_get_args()
            );
        }
    }

    /**
     * Get instance
     *
     * @return self
     */
    final public static function getInstance()
    {
        return self::$_inInstance === null ? self::$_inInstance = new static(func_get_args()) : self::$_inInstance;
    }

    /**
     * Wrapper of static::getInstance
     *
     * @return self
     */
    final public static function getStatic()
    {
        return self::getInstance();
    }
}
