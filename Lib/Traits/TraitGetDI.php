<?php

namespace Phalcon\Eyas\Traits;

/**
 * Class TraitGetDI
 * @package Phalcon\Eyas\Traits
 */
trait TraitGetDI
{

    /*
     *
     */
    public static function getDI()
    {
        return \Phalcon\DI::getDefault();
    }
}
