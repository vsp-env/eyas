<?php

namespace  Phalcon\Eyas\Traits;

/**
 * Class TraitWhoAmI
 * @package VSP\Aux\Origin\Traits
 */
trait TraitWhoAmI
{

    /** @var null $WhoAmI */
    protected static $WhoAmI = null;

    /** @var null $WhoIsMyNS */
    protected static $WhoIsMyNS = null;

    /**
     * Get class name
     *
     * @return string
     */
    public static function whoAmI()
    {
        $string = get_called_class();
        $strPos = strrpos($string, '\\');

        if ($strPos !== false) {
            $string = substr($string, $strPos + 1);
        }

        return self::$WhoAmI = $string;
    }

    /**
     * Get class name
     *
     * @return string
     */
    public static function whoIs($alias)
    {
        $string = $alias;
        $strPos = strrpos($alias, '\\');

        if ($strPos !== false) {
            return substr($string, $strPos + 1);
        }

        return $string;
    }

    /**
     * Get class name
     *
     * @return string
     */
    public static function whoIsMyNS($o = 0)
    {
        $string = get_called_class();
        $strPos = strrpos($string, '\\');

        if ($strPos !== false) {
            return self::$WhoIsMyNS = substr($string, 0, $strPos + $o);
        }

        return null;
    }
}
