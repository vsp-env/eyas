<?php

namespace Phalcon\Eyas\Traits;

/**
 * Class TraitGetInstance
 * @package Phalcon\Eyas\Traits
 */
trait TraitGetInstance
{

    /** @var null $_inInstance */
    private static $_inInstance = null;

    /**
     * Get new instance
     *
     * @return static
     */
    public static function getInstance($param = null)
    {
        $result = new \ReflectionClass(get_called_class());

        return $result->newInstanceArgs(
            is_array($param) ?
                $param :
                func_get_args()
        );
    }

    /**
     * Get shared instance
     *
     * @return static
     */
    public static function getInstanceShared()
    {
        return self::$_inInstance === null ? self::$_inInstance = static::getInstance(func_get_args()) : self::$_inInstance;
    }

    /**
     * Set shared instance
     *
     * @return static
     */
    public static function setInstanceShared()
    {
        return self::$_inInstance = static::getInstance(func_get_args());
    }

    /**
     * Wrapper of static::getInstanceShared
     *
     * @return TraitGetInstance
     */
    public static function getStatic()
    {
        return static::getInstanceShared();
    }
}
