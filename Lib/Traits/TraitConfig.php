<?php

namespace Phalcon\Eyas\Traits;

/**
 * Class TraitConfig
 * @package Phalcon\Eyas\Traits
 */
trait TraitConfig
{

    /** @var mixed $_inConfig */
    private $_inConfig;

    /**
     *
     */
    public function getConfig($k = null)
    {
        return $k ? $this->getDI()->getConfig()->{ $k } : $this->getDI()->getConfig();
    }

    /**
     *
     */
    public function setConfig($v)
    {
        return $this->_inConfig = $v;
    }

    /**
     *
     */
    public function getConfigSection($k = null)
    {
        if ($this->_inConfig === null) {
            $this->_inConfig = $this->getDI()->getConfig();
        }

        return $k ? $this->_inConfig->{ $k } : $this->_inConfig;
    }
}
