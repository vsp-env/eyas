<?php

namespace Phalcon\Eyas\Traits;

/**
 * Class TraitContainer
 * @package Phalcon\Eyas\Traits
 */
trait TraitContainer
{

    /** @var array $_inContainer */
    private $_inContainer = [];

    /**
     * Get value
     *
     * @return mixed|null
     */
    public function containerGet($k, $vDefault = null)
    {
        return isset($this->_inContainer[$k]) === false ? $vDefault : $this->_inContainer[$k];
    }

    /**
     * Check existence
     *
     * @return bool
     */
    public function containerExists($k)
    {
        return isset($this->_inContainer[$k]);
    }

    /**
     * Set value
     *
     * @return mixed
     */
    public function containerSet($k, $v)
    {
        return $this->_inContainer[$k] = $v;
    }

    /**
     * Get reference to an container array
     *
     * @return array
     */
    public function&containerGetEntire()
    {
        return $this->_inContainer;
    }

    /**
     * Get reference to an container value
     *
     * @return mixed
     */
    public function&containerGetReferenced($k)
    {
        return $this->_inContainer[$k];
    }
}
