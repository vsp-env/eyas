<?php

namespace Phalcon\Eyas\Server\Base;

/**
 * Class HTTPStreams
 * @package Phalcon\Eyas\Server\Base
 */
class HTTPStreams implements \Countable
{

    /*
     *
     */
    protected
        $container = [],
        $index = 10000000;

    /**
     *
     */
    protected function onClose($value)
    {
        return true;
    }

    /**
     *
     */
    public function count()
    {
        return count($this->container);
    }

    /**
     *
     */
    public function add($req, $res)
    {
        $this->container[$this->index ++] = (object) [
            'response' => $res,
            'request' => $req,
            'time' => time(),
            'timeout' => 60,
        ];

        return $this->index - 1;
    }

    /**
     *
     */
    public function close($i, $force = false)
    {
        if (isset($this->container[$i])) {
            if ($force || $this->onClose($this->container[$i])) {
                unset($this->container[$i]);
            }
        }

        return $this;
    }

    /**
     *
     */
    public function ensure()
    {
        foreach ($this->container as $I => $V) {
            if ($V->time + $V->timeout <= time()) {
                $this->close($I);
            }
        }

        return $this;
    }

    /**
     *
     */
    public function updateTime($i)
    {
        if (isset($this->container[$i])) {
            $this->container[$i]->time = time();
        }

        return $this;
    }

    /**
     *
     */
    public function updateTimeout($i, $value)
    {
        if (isset($this->container[$i])) {
            $this->container[$i]->timeout = $value;
        }

        return $this;
    }
}
