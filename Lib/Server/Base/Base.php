<?php

namespace Phalcon\Eyas\Server\Base;

use Phalcon\Eyas\Eyas\EyasModule;

/**
 * Class HTTP
 * @package Phalcon\Eyas\Server\Base
 */
class HTTP
{

    /*
     *
     */
    public
        $globalCookie = [],
        $globalGet = [],
        $globalPost = [],
        $globalPostRaw = null,
        $globalPut = [],
        $globalSession = [];

    /**
     * @param $value
     */
    public function setCookie($value)
    {
        $_COOKIE = $this->globalCookie = (new \http\Cookie($value))->getCookies();

        return $this;
    }

    /**
     * @param $value
     */
    public function setGet($value)
    {
        $_GET = $this->globalGet = $value;

        return $this;
    }

    /**
     * @param $value
     */
    public function setPost($value)
    {
        $_POST = $this->globalPost = $value;

        return $this;
    }

    /**
     * @param $value
     */
    public function setPostRaw($value)
    {
        $_POST = $this->globalPostRaw = $value;

        return $this;
    }

    /**
     * @param $value
     */
    public function setPut($value)
    {
        $_POST = $this->globalPut = $value;

        return $this;
    }

    /**
     * @param $value
     */
    public function setSession($value)
    {
        $_SESSION = $this->globalSession = [];

        return $this;
    }
}

/**
 * Class Base
 * @package Phalcon\Eyas\Server\Base
 */
class Base extends EyasModule
{

    /*
     *
     */
    protected
        $application,
        $http,
        $host = '0.0.0.0',
        $port = 80,
        $requests = [],
        $optBaseDir = null,
        $optShowNotices = true,
        $optStandalone = false;

    /**
     *
     */
    protected function onRequest($req, $res)
    {

    }

    /**
     *
     */
    public static function showMessage($value)
    {
        fwrite(STDOUT, date('Y-m-d H:i:s') . ' | ' . $value . "\n");
    }

    /**
     *
     */
    public static function showMessageError($value)
    {
        fwrite(STDERR, date('Y-m-d H:i:s') . ' | ' . $value . "\n");
    }

    /**
     *
     */
    public function __construct(\Phalcon\Eyas\Application\Base\Base $application)
    {
        $this->getDI()->setShared('server', $this);

        $this->application = $application;
        $this->http = new HTTP();

        $this->setBaseDir($this->application->getConfig()->baseDir);
    }

    /**
     *
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     *
     */
    public function getHTTP()
    {
        return $this->http;
    }

    /**
     *
     */
    public function setBaseDir($value)
    {
        $this->optBaseDir = realpath($value);

        return $this;
    }

    /**
     *
     */
    public function setShowNotices($value = true)
    {
        $this->optShowNotices = $value;

        return $this;
    }

    /**
     *
     */
    public function setStandalone($value)
    {
        $this->optStandalone = true;

        return $this->setBaseDir($value);
    }

    /**
     *
     */
    public function listen($port = 80, $host = '0.0.0.0')
    {

    }
}
