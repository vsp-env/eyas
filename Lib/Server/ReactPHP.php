<?php

namespace Phalcon\Eyas\Server;

use \Phalcon\Eyas\Server\Base\Base;

/**
 * Class HTTPStreams
 * @package Phalcon\Eyas\Server
 */
class HTTPStreams extends \Phalcon\Eyas\Server\Base\HTTPStreams
{

    /**
     *
     */
    protected function onClose($value)
    {
        $value->response->close();
        $value->request->close();

        return true;
    }
}

/**
 * Class ReactPHP
 * @package Phalcon\Eyas\Server
 */
class ReactPHP extends Base
{

    /** @var bool $optPhalcon1x */
    protected $optPhalcon1x = false;

    /** @var mixed $eventLoop */
    protected $eventLoop;

    /**
     * Handle request.
     *
     * @param \React\Http\Request $req React request
     * @param \React\Http\Response $res React response
     */
    protected function doRequest($req, $res)
    {
        $appResponse = $this->application->response;
        $retry = 2;

        $this->application->onRefresh();

        do {
            @ ob_end_clean();

            $cookies = $appResponse->getCookies();

            if ($cookies) {
                $cookies->reset();
            }

            $headers = $appResponse->getHeaders();

            if ($headers) {
                $headers->reset();
            }

            $session = $this->application->session;

            if ($session) {
                $session->start();
            }

            try {
                $resBody = $this->application->run($req->getPath());

                $headers = $appResponse->getHeaders()->toArray();

                // remove first row as http status (such as "HTTP/1.0 200 OK")
                if (empty($headers) === false) {
                    array_shift($headers);
                } else {
                    $headers = [];
                }

                if ($cookies && $cookies->count()) {
                    $headers['Set-Cookie'] = $cookies->toArrayFormatted(true);
                }

                if ($session) {
                    $session->close();
                }

                $res->writeHead(
                    $this->optPhalcon1x ? (isset($headers['Status']) ? $headers['Status'] : 200) : $appResponse->getStatusCode(),
                    $headers
                );

                break;
            } catch (\Exception $e) {
                if ($this->optShowNotices) {
                    static::showMessageError('Exception raised: ' . get_class($e) . ' on ' . $e->getMessage() . ' ( ' . $e->getCode() . ' )');
                }

                if ($retry -- > 0 && $this->application->onException($e) === true) {
                    continue;
                }

                $resBody = null;

                if ($session) {
                    $session->close();
                }

                $res->writeHead(500);

                break;
            }
        } while ($retry > 0);

        $res->end($resBody);
    }

    /**
     * Event: on request (http)
     *
     * @param \React\Http\Request $req React request
     * @param \React\Http\Response $res React response
     */
    protected function onRequest($req, $res)
    {
        $header = $req->getHeaders();

        $this->http->
            setCookie(isset($header['Cookie']) ? $header['Cookie'] : [])->
            setGet($req->getQuery())->
            setPost([])->
            setSession([]);

        // if body is going to transmit
        if (isset($header['Content-Length'])) {
            $buffer = '';
            $length = $header['Content-Length'] + 0;
            $stream = $this->httpStreams->add($req, $res);

            $req->on(
                'data',
                function($d) use (
                    $req,
                    $res,
                  & $buffer,
                  & $length,
                    $header,
                    $stream
                ) {
                    $this->httpStreams->updateTime($stream);

                    $buffer.= $d;
                    $length-= strlen($d);

                    if ($length <= 0) {
                        $this->application->request->setRawBody($buffer);
                        
                        if (isset($header['Content-Type'])) {
                            switch ($header['Content-Type']) {
                                case 'application/x-www-form-urlencoded':
                                    parse_str($buffer, $_POST);

                                    break;
                            }
                        } else {
                            $this->application->request->setRawBody($buffer);
                        }

                        $this->doRequest($req, $res);

                        $this->httpStreams->close($stream, true);
                    }
                }
            );
        } else {
            $this->doRequest($req, $res);
        }
    }

    /**
     *
     */
    public function __construct(\Phalcon\Eyas\Application\Base\Base $application)
    {
        parent::__construct($application);

        $this->httpStreams = new HTTPStreams();
    }

    /**
     *
     */
    public function setPhalcon1x($value = true)
    {
        $this->optPhalcon1x = $value;

        return $this;
    }

    /**
     *
     */
    public function listen($port = 80, $host = '0.0.0.0')
    {
        static::showMessage('--------------------------------');
        static::showMessage('');
        static::showMessage('    R E A C T  P H A L C O N    ');
        static::showMessage('');
        static::showMessage('--------------------------------');

        $this->eventLoop = \React\EventLoop\Factory::create();

        $socket = new \React\Socket\Server($this->eventLoop);

        $server = new \React\Http\Server($socket);

        $server->on(
            'request',
            $this->optStandalone ?
                function($req, $res) {
                    $_file = $this->optBaseDir . rawurldecode($req->getPath());

                    if (file_exists($_file) && substr(realpath($_file), 0, strlen($this->optBaseDir)) === $this->optBaseDir) {
                        $res->writeHead();
                        $res->end(file_get_contents($_file));
                    } else {
                        $this->onRequest($req, $res);
                    }
                } :
            $this->getMethod('onRequest')
        );

        $socket->listen($port, $host);

        $this->eventLoop->addPeriodicTimer(
            10,
            function() {
                if ($this->optShowNotices) {
                    static::showMessage(number_format(memory_get_usage(1) / 1024 / 1024, 1) . ' MB, ' . $this->httpStreams->count() . ' req. active');
                }

                $this->httpStreams->ensure();
            }
        );

        $this->setPhalcon1x(method_exists($this->application->response, 'getStatusCode') === false);

        $this->eventLoop->run();
    }

}
