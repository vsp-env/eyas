<?php

namespace Phalcon\Eyas\Http\Request;

/**
 * Class Request
 * @package Phalcon\Eyas\Http\Response
 */
class Request extends \Phalcon\Http\Request
{

    /**
     *
     */
    public function setRawBody($value)
    {
        $this->_rawBody = $value;

        return $this;
    }
}
