<?php

namespace Phalcon\Eyas\Http\Response;

/**
 * Class Cookies
 * @package Phalcon\Eyas\Http\Response
 */
class Cookies extends \Phalcon\Http\Response\Cookies
{

    /**
     *
     */
    public function count()
    {
        return count($this->_cookies);
    }

    /**
     *
     */
    public function toArray()
    {
        return $this->_cookies;
    }

    /**
     *
     */
    public function toArrayFormatted($forceSession = false)
    {
        $result = [];

        foreach ($this->_cookies as $I => $V) {
            $result[] =
                $I . '=' .
                $V->getValue() . '; ' .
                ($V->getExpiration() ? ('expires=' . gmdate('D, d M Y H:i:s T', $V->getExpiration()) . '; ') : null) .
                'path=' . $V->getPath();
        }

        if ($forceSession) {
            $session = $this->getDI()->getSession();

            if ($session->isStarted()) {
                $result[] = $session->asCookie();
            }
        }

        return $result;
    }
}
