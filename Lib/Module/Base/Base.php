<?php

namespace Phalcon\Eyas\Module\Base;

use Phalcon\DiInterface;
use Phalcon\Eyas\Extra\Injector\Traits\TraitInject;
use Phalcon\Eyas\Eyas\EyasModule;
use Phalcon\Eyas\Traits\TraitWhoAmI;

/**
 * Class Base
 * @package Phalcon\Eyas\Modules\Base
 */
class Base extends EyasModule implements \Phalcon\Mvc\ModuleDefinitionInterface
{
    use TraitInject;

    /**
     *
     */
    protected function onConfigInclude($config)
    {
        $module = $config->modules->list[$this->router->getModuleName()];

        $this->dispatcher->setDefaultNameSpace($config->ns . '\\' . $module . '\\Controllers');

        return include $config->baseDir . 'applications/' . $module . '/Config/config.php';
    }

    /**
     *
     */
    protected function onInitRoutes()
    {

    }

    /**
     * Register specific autoloader
     */
    public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null)
    {
        $this->loaderEyas->loadModuleConfig($this->onConfigInclude($this->getConfig()->application));
    }

    /**
     * Register specific services
     */
    public function registerServices(\Phalcon\DiInterface $dependencyInjector = null)
    {

    }
}
