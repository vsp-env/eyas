<?php

namespace Phalcon\Eyas\Module;

use Phalcon\DiInterface;
use Phalcon\Eyas\Module\Base\Base;
use Phalcon\Eyas\Traits\TraitConfig;

/**
 * Class Module
 * @package Phalcon\Eyas\Module
 */
abstract class Module extends Base
{
    use TraitConfig;
}
