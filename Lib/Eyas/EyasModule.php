<?php

namespace Phalcon\Eyas\Eyas;

use Phalcon\Eyas\Traits\TraitGetParam;
use Phalcon\Eyas\Traits\TraitGetterSetterPhalcon;
use Phalcon\Eyas\Traits\TraitWhoAmI;

/**
 * Class EyasModule
 * @package Phalcon\Eyas\Eyas
 */
abstract class EyasModule extends \Phalcon\Mvc\User\Component
{
    use TraitGetParam,
        TraitGetterSetterPhalcon,
        TraitWhoAmI;

    /** @var $_config */
    protected $_config;

    /**
     *
     */
    public function __construct()
    {
        $this->initialize();
    }

    /**
     *
     */
    public function getConfig($alias = null)
    {
        return $this->_config
            ? $this->_config
            : $this->_config = $alias
                ? $this->__get('config')->{ $alias }
                : $this->__get('config');
    }

    /**
     *
     */
    public function getMethod($alias)
    {
        if (method_exists($this, $alias)) {
            $method = new \ReflectionMethod($this, $alias);

            return $method->getClosure($this);
        }

        throw new \Exception('Method not exists: ' . $alias);
    }

    /**
     *
     */
    public function getKey($k, $d = null)
    {
        $config = $this->getConfig();

        foreach (explode('.', $k) as $k) {
            if (isset($config[$k])) {
                $config = $config[$k];
            } else {
                return $d;
            }
        };

        return $config;
    }

    /**
     *
     */
    public function hasKey($k, $d = null)
    {
        $config = $this->getConfig();

        foreach (explode('.', $k) as $k) {
            if (isset($config[$k])) {
                $config = $config[$k];
            } else {
                return false;
            }
        };

        return true;
    }

    /**
     *
     */
    public function initialize()
    {

    }
}
