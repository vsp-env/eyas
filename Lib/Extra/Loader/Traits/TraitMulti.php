<?php

namespace Phalcon\Eyas\Extra\Loader\Traits;

/**
 * Class TraitMulti
 * @package Phalcon\Eyas\Extra\Loader\Traits
 */
trait TraitMulti
{

    /**
     *
     */
    protected static function onLoad($config)
    {
        if (isset($config->autoload)) {
            $config = $config->autoload;

            if (isset($config->directories)) {
                static::$_loader->registerDirs(
                    (array) $config->directories,
                    true
                )->register();
            }

            if (isset($config->ns)) {
                static::$_loader->registerNamespaces(
                    (array) $config->ns,
                    true
                )->register();
            }
        }

        return static::$_di;
    }
}
