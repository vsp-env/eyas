<?php

namespace Phalcon\Eyas\Extra\Loader;

use Phalcon\DiInterface;
use Phalcon\Eyas\Extra\Injector\Injector;

/**
 * Class Loader
 * @package Phalcon\Eyas\Extra\Loader
 */
class Loader
{

    protected static
        $_config,
        $_di,
        $_injector,
        $_loader;

    /**
     *
     */
    protected static function onGetDI()
    {
        return new \Phalcon\DI\FactoryDefault();
    }

    /**
     *
     */
    protected static function onGetLoader()
    {
        return new \Phalcon\Loader();
    }

    /**
     *
     */
    protected static function onGetInjector()
    {
        return new Injector();
    }

    /**
     *
     */
    protected static function onLoad($config)
    {
        if (isset($config->autoload)) {
            static::$_loader->registerDirs((array) $config->autoload)->register();
        }

        return static::$_di;
    }

    /**
     *
     */
    public static function getConfig()
    {
        return static::$_config;
    }

    /**
     *
     */
    public static function getDI()
    {
        return static::$_di ? static::$_di : static::$_di = static::onGetDI();
    }

    /**
     *
     */
    public static function getInject()
    {
        return static::$_injector;
    }

    /**
     *
     */
    public static function getLoader()
    {
        return static::$_loader;
    }

    /**
     *
     */
    public static function load($config)
    {
        static::$_config = $config;

        static::$_loader = static::onGetLoader();

        static::$_di = static::onGetDI();

        static::$_di->setShared(
            'config',
            function() use ($config) {
                return $config;
            }
        );
        static::$_di->setShared(
            'configApp',
            function() use ($config) {
                return $config;
            }
        );
        static::$_di->setShared(
            'loaderEyas',
            function() use ($config) {
                return new static();
            }
        );

        static::$_injector = static::onGetInjector()->inject($config->application);

        return static::onLoad($config->application);
    }

    /**
     *
     */
    public function loadModuleConfig($config)
    {
        static::$_di->setShared(
            'config',
            function() use ($config) {
                return $config;
            }
        );

        static::$_injector->inject($config->application);

        return static::onLoad($config->application);
    }
}
