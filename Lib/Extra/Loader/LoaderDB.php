<?php

namespace Phalcon\Eyas\Extra\Loader;

use Phalcon\DiInterface;

/**
 * Class LoaderDB
 * @package Phalcon\Eyas\Extra\Loader
 */
class LoaderDB extends Loader
{

    /**
     *
     */
    protected static function onGetAdapterDB($param)
    {
        return new \Phalcon\Db\Adapter\Pdo\Mysql($param);
    }

    /**
     *
     */
    protected static function onGetAdapterMetaData()
    {
        return new \Phalcon\Mvc\Model\Metadata\Memory();
    }

    /**
     *
     */
    protected static function onLoad()
    {

        if (isset(static::$_config->database->db)) {
            foreach (static::$_config->database as $I => $V) {
                static::$_di->set(
                    $I,
                    function () use ($I, $V)
                    {
                        return static::onGetAdapterDB([
                            'host' => $V->host,
                            'username' => $V->username,
                            'password' => $V->password,
                            'dbname' => $V->dbname,
                            'charset' => $V->charset,
                        ]);
                    }
                );
            }
        } else {
            static::$_di->set(
                'db',
                function ()
                {
                    return static::onGetAdapterDB([
                        'host' => static::$_config->database->host,
                        'username' => static::$_config->database->username,
                        'password' => static::$_config->database->password,
                        'dbname' => static::$_config->database->dbname,
                        'charset' => static::$_config->database->charset,
                    ]);
                }
            );
        }

        static::$_di->set(
            'modelsMetadata',
            function () {
                return static::onGetAdapterMetaData();
            }
        );

        return parent::onLoad();
    }

    /**
     *
     */
    public static function getDB($param)
    {
        return static::onGetAdapterDB($param);
    }
}
