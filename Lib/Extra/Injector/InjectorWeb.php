<?php

namespace Phalcon\Eyas\Extra\Injector;

use Phalcon\DiInterface;
use Phalcon\Eyas\Http\Response\Cookies;
use Phalcon\Eyas\Session\Adapter\Files;


/**
 * Class InjectorWeb
 * @package Phalcon\Eyas\Extra\Injector
 */
class InjectorWeb extends Injector
{

    /**
     *
     */
    protected function onInjectView()
    {
        $config = $this->getConfig('application')->bundles->view;

        $entity = new \Phalcon\Mvc\View();

        $entity->setViewsDir($config->directory->views);

        $entity->registerEngines([
            '.volt' => function ($_entity) use ($config) {
                $_engine = new \Phalcon\Mvc\View\Engine\Volt($_entity, $this->di);

                $_engine->setOptions([
                    'compiledPath'  => $config->directory->cache,
                    'compileAlways' => $config->get('compileAlways', false) === true,
                ]);

                return $_engine;
            }
        ]);

        return $entity;
    }

    /**
     *
     */
    protected function onInjectCookies()
    {
        $entity = new Cookies();

        $entity->useEncryption($this->getConfig('application')->bundles->cookies->get('encrypted') === true);

        return $entity;
    }

    /**
     *
     */
    protected function onInjectCrypt()
    {
        $entity = new \Phalcon\Crypt();

        $entity->setKey($this->getConfig('application')->bundles->crypt->get('key') === true);

        return $entity;
    }

    /**
     *
     */
    protected function onInjectSessionFiles()
    {
        return $this->di->setShared('session', function() {
            $entity = new \Phalcon\Eyas\Session\Adapter\Files();

            $this->getConfig('application')->get('domain')
                ? $entity->setDomain($this->getConfig('application'))->start()
                : $entity->start();

            return $entity;
        });
    }

    /**
     *
     */
    protected function onInjectURL()
    {
        $entity = new \Phalcon\Mvc\Url();

        $entity->setBaseUri($this->getConfig()->bundles->uri->base);

        return $entity;
    }
}
