<?php

namespace Phalcon\Eyas\Extra\Injector\Traits;

use Phalcon\DiInterface;

/**
 * Class TraitInject
 * @package Phalcon\Eyas\Extra\Injector\Traits
 */
trait TraitInject
{

    /**
     *
     */
    public function inject(DiInterface $di)
    {
        foreach ($this->getConfig()->injections as $k => $v) {
            $method = is_numeric($k) ? 'onInject' . $v : 'onInject' . $k;

            $di->setShared($v, (new \ReflectionMethod($this, $method))->getClosure($this));
        }

        return true;
    }
}
