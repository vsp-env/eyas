<?php

namespace Phalcon\Eyas\Extra\Injector;

use Phalcon\DiInterface;
use Phalcon\Eyas\Extra\Injector\Base\Base;
use Phalcon\Eyas\Http\Request\Request;

/**
 * Class Injector
 * @package Phalcon\Eyas\Extra\Injector
 */
class Injector extends Base
{

    /**
     *
     */
    protected function onInjectDispatcher()
    {
        return new \Phalcon\Mvc\Dispatcher();
    }

    /**
     *
     */
    protected function onInjectAcl()
    {
        return new \Phalcon\Eyas\PlugIn\Acl\Adapter\NativeInclude();
    }

    /**
     *
     */
    protected function onInjectAclConfig()
    {
        return new \Phalcon\Eyas\PlugIn\Acl\Adapter\Config();
    }

    /**
     *
     */
    protected function onInjectAclJson()
    {
        return new \Phalcon\Eyas\PlugIn\Acl\Adapter\JSON();
    }

    /**
     *
     */
    protected function onInjectEventsManager()
    {
        return $this->di->getShared('eventsManager');
    }

    /**
     *
     */
    protected function onInjectLocalization()
    {
        return new \Phalcon\Eyas\PlugIn\Localization\Localization();
    }

    /**
     *
     */
    protected function onInjectRequest()
    {
        return new Request();
    }

    /**
     *
     */
    protected function onInjectRouter()
    {
        return new \Phalcon\Mvc\Router(true);
    }

    /**
     *
     */
    protected function onInjectTranslation()
    {
        $this->di->setShared('t', function() { return new \Phalcon\Eyas\PlugIn\Translation\Adapter\NativeInclude(); });
    }

    /**
     *
     */
    protected function onInjectTranslationConfig()
    {
        $this->di->setShared('t', function() { return new \Phalcon\Eyas\PlugIn\Translation\Adapter\Config(); });
    }

    /**
     *
     */
    protected function onInjectTranslationEnv()
    {
        $this->di->setShared('t', function() { return new \Phalcon\Eyas\PlugIn\Translation\Adapter\Env(); });
    }

    /**
     *
     */
    protected function onInjectTranslationJson()
    {
        $this->di->setShared('t', function() { return new \Phalcon\Eyas\PlugIn\Translation\Adapter\Json(); });
    }

    /**
     *
     */
    protected function onInjectUserSession()
    {
        $this->di->setShared('user', function() { return new \Phalcon\Eyas\PlugIn\User\Adapter\Session($this); });
    }
}
