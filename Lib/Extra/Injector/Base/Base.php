<?php

namespace Phalcon\Eyas\Extra\Injector\Base;

use Phalcon\Eyas\Traits\TraitConfig;

/**
 * Class Injector
 * @package Phalcon\Eyas\Extra\Injector
 */
class Base extends \Phalcon\Mvc\User\Component
{
    use TraitConfig;

    /**
     *
     */
    public function inject($config = null)
    {
        if ($config === null) {
            $config = $this->getConfig('application');
        } else {
            $config = $this->setConfig($config);
        }

        if (isset($config->injections)) {
            foreach ($config->injections as $k => $v) {
                if (is_numeric($k)) {
                    $_method = 'onInject' . $v;

                    $this->di->setShared($v, (new \ReflectionMethod($this, $_method))->getClosure($this));
                } else {
                    $_method = 'onInject' . $k;

                    $this->{ $_method . $v }();
                }
            }
        }

        return $this;
    }
}
