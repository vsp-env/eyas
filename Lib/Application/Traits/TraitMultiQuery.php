<?php

namespace Phalcon\Eyas\Application\Traits;

use Phalcon\DiInterface;

/**
 * Class TraitMultiQuery
 * @package Phalcon\Eyas\Application\Traits
 */
trait TraitMultiQuery
{

    /**
     *
     */
    protected function onRegister()
    {
        $_entity = $this->router;

        $_entity->add('/', [
            'controller'        => $this->getConfig()->preferences->defaultRoute->controller,
            'action'            => $this->getConfig()->preferences->defaultRoute->action,
        ]);

        $_entity->notFound([
            'controller'        => $this->getConfig()->preferences->defaultRoute->controller,
            'action'            => $this->getConfig()->preferences->defaultRoute->action,
        ]);

        $_entity->setDefaults([
            'controller'        => $this->getConfig()->preferences->defaultRoute->controller,
            'action'            => $this->getConfig()->preferences->defaultRoute->action,
        ]);

        return $_entity;
    }

    /**
     *
     */
    public function getModuleRedirect(
        $route,
        $param = null
    )
    {
        $_config = $this->getConfig();

        if (isset($_config->modules->redirects[$route])) {
            $_module = $_config->modules->redirects[$route];

            if ($_module instanceof \Phalcon\Config) {
                return $_config->protocol . '://' . $_module[0] . $_config->domain . '/' . $_module[1];
            }

            return $_config->protocol . '://' . $route . $_config->domain . '/' . $_module;
        }

        return null;
    }

    /**
     *
     */
    public function run($route = null)
    {
        if ($this->isRunning === false) {
            $_config = $this->getConfig();

            foreach ($_config->modules->list as $i => $v) {
                $this->registerModules(
                    [
                        $i => [
                            'className'     => $_config->ns . '\\' . $v . '\\Module',
                        ]
                    ],
                    true
                );
            }

            $_domain = strpos($_SERVER['HTTP_HOST'], $_config->domain);

            if ($_domain !== false) {
                $this->router->setDefaultModule(substr($_SERVER['HTTP_HOST'], 0, $_domain));
            } else {
                $this->router->setDefaultModule($_config->preferences->defaultModule);
            }

            $this->onRegister();
            $this->onInitRoutes();

            $this->isRunning = true;
        }

        return $this->onContentOutput($this->handle($route)->getContent());
    }
}
