<?php

namespace Phalcon\Eyas\Application\Traits;

use Phalcon\DiInterface;

/**
 * Class TraitMulti
 * @package Phalcon\Eyas\Application\Traits
 */
trait TraitMulti
{

    /**
     *
     */
    protected function onRegister()
    {
        $_entity = $this->router;

        $_entity->add('/', [
            'controller'        => $this->getConfig()->preferences->defaultRoute->controller,
            'action'            => $this->getConfig()->preferences->defaultRoute->action,
        ]);

        $_entity->notFound([
            'controller'        => $this->getConfig()->preferences->defaultRoute->controller,
            'action'            => $this->getConfig()->preferences->defaultRoute->action,
        ]);

        $_entity->setDefaults([
            'controller'        => $this->getConfig()->preferences->defaultRoute->controller,
            'action'            => $this->getConfig()->preferences->defaultRoute->action,
        ]);

        return $_entity;
    }

    /**
     *
     */
    public function run($route = null)
    {
        if ($this->isRunning === false) {
            $this->di->setShared('application', $this);

            $_config = $this->getConfig();

            foreach ($_config->modules->list as $I => $V) {
                $this->registerModules(
                    [
                        $I => [
                            'className'     => $_config->ns . '\\' . $V . '\\Module',
                        ]
                    ],
                    true
                );
            }

            $this->router->setDefaultModule($_config->preferences->defaultModule);

            $this->onRegister();
            $this->onInitRoutes();

            $this->isRunning = true;
        }

        return $this->onContentOutput($this->handle($route)->getContent());
    }
}
