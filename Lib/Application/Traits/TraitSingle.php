<?php

namespace Phalcon\Eyas\Application\Traits;

use Phalcon\DiInterface;

/**
 * Class TraitSingle
 * @package Phalcon\Eyas\Application\Traits
 */
trait TraitSingle
{

    /**
     *
     */
    protected function onRegister()
    {
        $_entity = $this->router;

        $_entity->add('/', [
            'controller'        => $this->getConfig()->preferences->defaultRoute->controller,
            'action'            => $this->getConfig()->preferences->defaultRoute->action,
        ]);

        $_entity->notFound([
            'controller'        => $this->getConfig()->preferences->defaultRoute->controller,
            'action'            => $this->getConfig()->preferences->defaultRoute->action,
        ]);

        $_entity->setDefaults([
            'controller'        => $this->getConfig()->preferences->defaultRoute->controller,
            'action'            => $this->getConfig()->preferences->defaultRoute->action,
        ]);

        return $_entity;
    }

    /**
     *
     */
    public function run($route = null)
    {
        if ($this->isRunning === false) {
            $this->di->setShared('application', $this);

            $this->dispatcher->setDefaultNameSpace($this->getConfig()->ns . '\\Controllers');

            $this->onRegister();
            $this->onInitRoutes();

            $this->isRunning = true;
        }

        return $this->onContentOutput($this->handle($route)->getContent());
    }
}
