<?php

namespace Phalcon\Eyas\Application\Base;

use Phalcon\DiInterface;
use Phalcon\Eyas\Extra\Injector\Traits\TraitInject;
use Phalcon\Eyas\PlugIn\User\UserIdentityInterface;
use Phalcon\Eyas\Traits\TraitWhoAmI;

/**
 * Class Application
 * @package Phalcon\Eyas\Applications
 */
abstract class Base extends \Phalcon\Mvc\Application
{
    use TraitInject,
        TraitWhoAmI;

    /** @var bool $isRunning */
    protected $isRunning = false;

    /** @var UserIdentityInterface $user */
    protected $user;

    /**
     *
     */
    public function onException(\Exception $e)
    {

    }

    /**
     *
     */
    public function onRefresh()
    {

    }

    /**
     *
     */
    public function getUserByField($value)
    {

    }

    /**
     *
     */
    public function getUserById($id)
    {

    }

    /**
     *
     */
    public function run($route = null)
    {
        return null;
    }
}
