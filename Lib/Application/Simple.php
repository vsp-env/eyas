<?php

namespace Phalcon\Eyas\Application;

use Phalcon\Eyas\Application\Base\Base;

/**
 * Class Simple
 * @package Phalcon\Eyas\Application
 */
abstract class Simple extends Base
{

    /**
     *
     */
    protected function onContentOutput($output)
    {
        echo $output;
    }

    /**
     *
     */
    protected function onInitRoutes()
    {

    }

    /**
     *
     */
    protected function onRegister()
    {

    }

    /**
     * Get app's related config section.
     */
    public function getConfig()
    {
        return $this->di->getConfigApp()->application;
    }

    /**
     *
     */
    public function catchException(\Exception $e)
    {

    }

    /**
     *
     */
    public function run($route = null)
    {
        if ($this->isRunning === false) {
            $this->di->setShared('application', $this);

            $this->onRegister();
            $this->onInitRoutes();

            $this->isRunning = true;
        }

        return $this->onContentOutput($this->handle($route)->getContent());
    }
}
