<?php

namespace Phalcon\Eyas\Session\Adapter;

/**
 * Class Files
 * @package Phalcon\Eyas\Session\Adapter
 */
class Files extends \Phalcon\Session\Adapter\Files
{

    /**
     *
     */
    public function setDomain($value)
    {
        session_set_cookie_params(0, '/', $value);

        return $this;
    }
}
